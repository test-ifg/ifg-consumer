package id.vendi.model;

public class UploadFile {
  public String fileName;
  public String fileData;

  public UploadFile(String fileName, String fileData) {
    this.fileName = fileName;
    this.fileData = fileData;
  }
}
