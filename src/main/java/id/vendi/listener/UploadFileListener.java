package id.vendi.listener;

import id.vendi.model.UploadFile;
import io.smallrye.reactive.messaging.annotations.Broadcast;
import jakarta.enterprise.context.ApplicationScoped;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Base64;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ApplicationScoped
public class UploadFileListener {

  private static final Logger log = LoggerFactory.getLogger(UploadFileListener.class);

  @Incoming("ifg")
  @Broadcast
  public void uploadFile(UploadFile uploadFile) {
    byte[] decode = Base64.getDecoder().decode(uploadFile.fileData);

    try {
      this.writeFile(decode, uploadFile.fileName);
      log.info("Success writing file");
    } catch (IOException e) {
      log.error("Error writing file {}", e.getMessage());
    }
  }

  private void writeFile(byte[] content, String filename) throws IOException {

    File file = new File(filename);

    if (!file.exists()) {
      file.createNewFile();
    }

    FileOutputStream fop = new FileOutputStream(file);

    fop.write(content);
    fop.flush();
    fop.close();
  }
}
